array (
  'id' => '277015',
  'name' => 'REV- DSK- New Dental BL - CPC',
  'start_date' => '2017-07-11 14:00:00',
  'end_date' => '2017-07-11 17:00:00',
  'targeting_type' => 'topic',
  'enabled' => 'active',
  'status' => 'inactive',
  'bid_type' => 'cpc',
  'default_bid' => '0.10',
  'min_bid' => '0.01',
  'max_bid' => '0.40',
  'budget' => '100.00',
  'cost' => '197.46000000000000000000',
  'ctr' => '0.0875',
  'utm_codes' => 'target={adv_targets}&boosid={boost_id}&contid={content_id}&widgid={widget_id}',
  'exclude_low_volume' => 'off',
  'country_codes' => 
  array (
    0 => 'CA',
    1 => 'US',
  ),
  'country_targeting' => 'include',
  'optimize' => 'cpc',
  'language_targeting' => 
  array (
    0 => '1',
  ),
  'device_targeting' => 
  array (
    0 => '1',
  ),
)

{"id":"277015","enabled":"inactive","status":"disabled"}

array (
  'id' => '277015',
  'name' => 'REV- DSK- New Dental BL - CPC',
  'start_date' => '2017-07-11 14:00:00',
  'end_date' => '2017-07-11 17:00:00',
  'targeting_type' => 'topic',
  'enabled' => 'inactive',
  'status' => 'disabled',
  'bid_type' => 'cpc',
  'default_bid' => '0.10',
  'min_bid' => '0.01',
  'max_bid' => '0.40',
  'budget' => '100.00',
  'cost' => '197.46000000000000000000',
  'ctr' => '0.0875',
  'utm_codes' => 'target={adv_targets}&boosid={boost_id}&contid={content_id}&widgid={widget_id}',
  'exclude_low_volume' => 'off',
  'country_codes' => 
  array (
    0 => 'CA',
    1 => 'US',
  ),
  'country_targeting' => 'include',
  'optimize' => 'cpc',
  'language_targeting' => 
  array (
    0 => '1',
  ),
  'device_targeting' => 
  array (
    0 => '1',
  ),
)


{"id":"277015","enabled":"active","status":"inactive"}






{
	"id": "277015",
	"name": "REV- DSK- New Dental BL - CPC",
	"start_date": "2017-07-11 14:00:00",
	"end_date": "2017-07-11 17:00:00",
	"targeting_type": "topic",
	"enabled": "active",
	"status": "inactive",
	"bid_type": "cpc",
	"default_bid": 0.08,
	"min_bid": "0.08",
	"max_bid": "0.08",
	"budget": "100.00",
	"cost": "0.00",
	"ctr": "0.00",
	"utm_codes": "target={adv_targets}&boosid={boost_id}&contid={content_id}&widgid={widget_id}",
	"exclude_low_volume": "off",
	"country_codes": ["CA", "US"],
	"country_targeting": "include",
	"optimize": "cpc",
	"language_targeting": ["1"],
	"device_targeting": ["1"],
	"success": true
}

{
	"id": "277015",
	"name": "REV- DSK- New Dental BL - CPC",
	"start_date": "2017-07-11 14:00:00",
	"end_date": "2017-07-11 17:00:00",
	"targeting_type": "topic",
	"enabled": "active",
	"status": "inactive",
	"bid_type": "cpc",
	"default_bid": 0.1,
	"min_bid": "0.10",
	"max_bid": "0.10",
	"budget": "100.00",
	"cost": "0.00",
	"ctr": "0.00",
	"utm_codes": "target={adv_targets}&boosid={boost_id}&contid={content_id}&widgid={widget_id}",
	"exclude_low_volume": "off",
	"country_codes": ["CA", "US"],
	"country_targeting": "include",
	"optimize": "cpc",
	"language_targeting": ["1"],
	"device_targeting": ["1"],
	"success": true
}
