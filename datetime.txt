            //$offsetUTC = $form->get("offsetUTC");
            $nowUTC = new \DateTime("now", "UTC");
            $now = new \DateTime("now");
            $diff = $nowUTC->diff($now);
            if($diff->h !== 0){
                $time = $form->get("atTime");
                $interval = \DateInterval::createFromDateString("{$diff->h} hours");
                $time->add($interval);
            }
            
                        ->add('offsetUTC', HiddenType::class, [
                'mapped' => false,
                ]
            )

                if($('#appbundle_rule_offsetUTC').length === 1){
        var minutesOffset = new Date().getTimezoneOffset();
        var offset = (minutesOffset/60);
        $('#appbundle_rule_offsetUTC').val(offset);
    }
