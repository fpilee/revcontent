<?php


exec('ps aux', $output);
$running = false;
foreach ($output as $item){
    $pos = strpos($item, "jms-job-queue:run");
    if($pos){
       $running = true;
    }
}

if(!$running){
    exec("nohup /home/learnnowhub/public_html/scheduler/bin/console jms-job-queue:run --env=prod &", $output);
}