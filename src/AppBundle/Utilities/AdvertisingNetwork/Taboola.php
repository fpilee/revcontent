<?php


namespace AppBundle\Utilities\AdvertisingNetwork;

use AppBundle\Entity\Campaign;
use AppBundle\Entity\Setting;
use AppBundle\Utilities\Exceptions\ApiAuthenticationException;
use JMS\AopBundle\Exception\RuntimeException;
use Symfony\Component\Intl\Exception\NotImplementedException;


class Taboola extends CampaignManager{

    public $token;
    public $client;
    public $secret;

    public function __construct(Setting $client, Setting $secret, Setting $token)
    {
        $this->token = $token;
        $this->client = $client;
        $this->secret = $secret;
    }

    /**
     * @throws ApiAuthenticationException
     */
    private function reloadToken(){

        $client = new \GuzzleHttp\Client;

        $response = $client->request('POST', "https://backstage.taboola.com/backstage/oauth/token", [
            "http_errors" => false,
            'form_params' => [
                "client_id" => $this->client->getValue(),
                "client_secret" => $this->secret->getValue(),
                "grant_type" => "client_credentials",
            ],
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]

        ]);


        if($response->getStatusCode() !== 200){
            throw new ApiAuthenticationException("ContentAd token cannot be renewed, check API key/pass data in db");
        }
        $content = json_decode($response->getBody()->getContents(), TRUE);

        $this->token->setValue($content["access_token"]);
    }

    public function getCampaigns()
    {

        $client = new \GuzzleHttp\Client;


        $response = $client->request('GET', 'https://backstage.taboola.com/backstage/api/1.0/taboolaaccount-marlonphillipsdefendmedianet/campaigns/', [
            'headers' => [
                'Authorization' => " Bearer " . $this->token->getValue()
            ],
            "http_errors" => false
        ]);


        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getCampaigns();
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);
        $campaigns = $content["results"];

        $campaigns = $this->createCampaingsFromArray($campaigns);
        return $campaigns;

    }


    private function createCampaingsFromArray(array $campaignsArray){
        foreach($campaignsArray as $campaignArray){
             if($campaignArray["status"] === "RUNNING") {
            $campaign = new Campaign();
            $campaign->setName($campaignArray["name"]);
            $campaign->setId($campaignArray["id"]);
            $campaign->setSource("Taboola");
            $campaigns[] = $campaign;
             }
            
        }
        return $campaigns;
    }


    public function getStats($campaignId, $start, $end)
    {
        $client = new \GuzzleHttp\Client;

        $startDate = $start->format("Y-m-d");
        $endDate = $end->format("Y-m-d");

        $response = $client->request('GET', "https://backstage.taboola.com/backstage/api/1.0/taboolaaccount-marlonphillipsdefendmedianet/reports/campaign-summary/dimensions/campaign_site_day_breakdown", [
            'headers' => [
                'Authorization' => " Bearer " . $this->token->getValue()
            ],
            "http_errors" => false,
            'query' => [
                'start_date' => $startDate,
                'end_date' => $endDate
            ]
        ]);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getStats(null, $start, $end);
        }

        $content = json_decode($response->getBody()->getContents(), TRUE)["results"];

        return $this->formatStats($content, $start->format("m/d/Y"), $end->format("m/d/Y"));



    }

    public function updateCampaign($campaignId, $status)
    {
        // TODO: Implement updateCampaign() method.
    }

    private function formatStats($data, $start, $end){
        $formated = [];
        foreach ($data as $datum){
                if(floatval($datum["cpc"]) === 0.0 ){
                    continue;
                }

                $formated []=[
                    "campaign_id" => $datum["campaign"],
                    "start_date" => $start,
                    "end_date" => $end,
                    "ad_id" => $datum["site"],
                    "spend" => $datum["spent"],
                    "avg_cpc" =>$datum["cpc"],
                    "clicks" =>$datum["clicks"],
                    "impressions" =>$datum["impressions"],
                    "custom_var" => "siteid",
                    "network" => "TAB",
                ];
        }
        return $formated;
    }
}