<?php


namespace AppBundle\Utilities\AdvertisingNetwork;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\Setting;
use Symfony\Component\Intl\Exception\NotImplementedException;
use AppBundle\Utilities\Exceptions\ApiAuthenticationException;
use JMS\AopBundle\Exception\RuntimeException;

class ContentAd extends CampaignManager
{
    const CUSTOM_VARIABLE = "siteid";
    const AD_NETWORK = "ContentAd";

    public $token;
    public $client;
    public $secret;


    public function __construct(Setting $client, Setting $secret,Setting $token)
    {
        $this->token = $token;
        $this->client = $client;
        $this->secret = $secret;
    }

    public function getStats($campaignId, $start, $end)
    {

        $client = new \GuzzleHttp\Client;

        $startDate = $start->format("Y-m-d");
        $endDate = $end->format("Y-m-d");

        $response = $client->request('GET', "https://rest.content.ad/reports/advertiser/click?".
                "startDate=$startDate&endDate=$endDate&output=json&groupByDomain=true", [
            'auth' => [
                $this->client->getValue(),
                $this->secret->getValue()
            ],
            "http_errors" => false,
        ]);

        if($response->getStatusCode() === 401){
            throw new ApiAuthenticationException("ContentAd stats cannot be retrieved, check API key/pass data in db");
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);


        return $this->formatStats($content["rows"], $start->format("m/d/Y"), $end->format("m/d/Y"));
    }

    public function updateCampaign($campaignId, $status)
    {
        $validStatus = ["on" => "Active","off"=> "Paused"];
        $headers = [
            'auth_token' => $this->token->getValue(),
            'Content-Type' => 'application/json',
        ];
        $client = new \GuzzleHttp\Client();


        $body = ['campaign_id'=> $campaignId,'status' => $validStatus[$status]];

        $request = new \GuzzleHttp\Psr7\Request(
            'POST',
            "https://transact.content.ad/transact/1.0/advertiser/campaign/update",
            $headers,
            json_encode($body)
        );

        $response = $client->send($request);

        if($response->getStatusCode() === 429){
            //to many requests
            return false;
        }else if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->updateCampaign($campaignId,$status);
        }else if($response->getStatusCode() !== 200){
            throw new RuntimeException("Failed Job? rev" . $request->getBody()->getContents()); //change exception
        }
        return true;
    }

    public function getCampaigns()
    {
        $client = new \GuzzleHttp\Client;


        $response = $client->request('GET', "https://transact.content.ad/transact/1.0/advertiser/campaigns/list", [
            "http_errors" => false,
            'headers' => [
                'auth_token' => $this->token->getValue(),
            ]
        ]);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getCampaigns();
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);

        $campaings = $this->createCampaingsFromArray($content);

        return $campaings;
    }

    private function reloadToken(){

        $client = new \GuzzleHttp\Client;

        $response = $client->request('GET', "https://transact.content.ad/transact/1.0/advertiser/login", [
            "http_errors" => false,
            'auth' => [
                $this->client->getValue(),
                $this->secret->getValue()
            ]
        ]);

        if($response->getStatusCode() !== 200){
            throw new ApiAuthenticationException("ContentAd token cannot be renewed, check API key/pass data in db");
        }
        $content = json_decode($response->getBody()->getContents(), TRUE);

        $this->token->setValue($content["auth_token"]);
    }


    private function createCampaingsFromArray(array $campaignsArray){
        foreach($campaignsArray as $campaignArray){
            $campaign = new Campaign();
            $campaign->setName($campaignArray["name"]);
            $campaign->setId($campaignArray["campaign_id"]);
            $campaign->setSource("ContentAd");
            $campaigns[] = $campaign;
        }
        return $campaigns;
    }

    private function formatStats($data, $start, $end){

        foreach ($data as $datum){
            
            if(floatval($datum["spend"]) <= 0.0 ){
                continue;
            }
            
            $formated []=[
                "campaign_id" => $datum["campaign_id"],
                "ad_id" => $datum["domain_id"],
                "impressions" =>'',
                "clicks" =>$datum["clicks"],
                "spend" => $datum["spend"],
                "avg_cpc" =>$datum["ecpc"],
                "start_date" => $start,
                "end_date" => $end,
                "custom_var" => "siteid", //?? creaid,campid,siteid
                "network" => "CTA", //??
            ];
        }
        return $formated;
    }
}