<?php




namespace AppBundle\Utilities\AdvertisingNetwork;


use AppBundle\Entity\Setting;
use AppBundle\Entity\Campaign;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Intl\Exception\NotImplementedException;
use AppBundle\Utilities\Exceptions\ApiAuthenticationException;
use JMS\AopBundle\Exception\RuntimeException;


class MGID extends CampaignManager
{
    public $token;
    public $client;
    public $secret;

    public function __construct(Setting $client, Setting $secret, Setting $token)
    {
        $this->token = $token;
        $this->client = $client;
        $this->secret = $secret;
    }

    public function getCampaigns()
    {
        $client = new \GuzzleHttp\Client;


        $response = $client->request('GET', "https://api.mgid.com/v1/goodhits/clients/116146/campaigns", [
            "http_errors" => false,
            'query' => ['token' => $this->token->getValue()]
        ]);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getCampaigns();
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);

        if(!is_array($content)){
            sleep(10);
            return $this->getCampaigns();
        }

        $campaings = $this->createCampaingsFromArray($content);

        return $campaings;

    }

    public function getStats($campaignId, $start, $end)
    {

        $campaigns = $this->getCampaigns();
        $campaingStats = [];

        /**
         * @var Campaign $campaign
         */
        foreach ($campaigns as $campaign){
            $stats = $this->getRawStats($campaign->getId(), $start, $end);
            $campaingStats = array_merge($campaingStats, $this->formatStats($stats, $campaign->getId(), $start->format("m/d/Y"), $end->format("m/d/Y")));

        }
        return $campaingStats;
    }

    public function getRawStats($campaignId, $start, $end){
        $client = new \GuzzleHttp\Client;

        $response = $client->request('GET', "https://api.mgid.com/v1/goodhits/campaigns/$campaignId/quality-analysis/?", [
            "http_errors" => false,
            'query' => ['token' => $this->token->getValue(), 'dateInterval' => "interval", "startDate" => $start->format("Y-m-d"), "endDate" => $end->format("Y-m-d")]
        ]);
        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getRawStats($campaignId, $start, $end);
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);
        if($content != null && array_key_exists($campaignId, $content)){
            $startDate =$start->format("Y-m-d");
            $endDate =$end->format("Y-m-d");
            $key = "{$startDate}_{$endDate}";
            if(array_key_exists($key, $content[$campaignId])){
                return $content[$campaignId]["{$startDate}_{$endDate}"];
            }

        }
        return [];

    }
    public function updateCampaign($campaignId, $status)
    {
        // TODO: Implement updateCampaign() method.
    }

    private function reloadToken(){

        $email = $this->client->getValue();
        $password = $this->secret->getValue();
        $data = compact('email', 'password');

        $args = json_encode($data);
        $headers = [
            'Content-Type' => 'application/json'
        ];

        $client = new \GuzzleHttp\Client(["http_errors" => false]);

        $request = new \GuzzleHttp\Psr7\Request(
            'POST',
            "https://api.mgid.com/v1/auth/token",
            $headers,
            $args
        );

        $response = $client->send($request);

        if($response->getStatusCode() !== 200){
            throw new Exception("Cannot reload API Access Token for MGID");
        }
        $content = json_decode($response->getBody()->getContents(), TRUE);

        $this->token->setValue($content["token"]);
    }

    private function createCampaingsFromArray(array $campaignsArray){
        foreach($campaignsArray as $campaignArray){
            $campaign = new Campaign();
            $campaign->setName($campaignArray["name"]);
            $campaign->setId($campaignArray["id"]);
            $campaign->setSource("MGID");
            $campaigns[] = $campaign;
        }
        return $campaigns;
    }

    private function formatStats($ads, $boostID, $start, $end){

        $formated =[];
        foreach ($ads as $widget => $data){
            
            if(floatval($data["clicks"])>0){
                $avg_cpc = floatval($data["spent"])/floatval($data["clicks"]);
            }else{
                $avg_cpc = 0;
            }

            $formated []=[
                "campaign_id" => $boostID,
                "ad_id" => $widget,
                "start_date" => $start,
                "end_date" => $end,
                "impressions" => '',
                "clicks" =>$data["clicks"],
                "spend" => $data["spent"],
                "avg_cpc" => $avg_cpc,
                "custom_var" => "widgetid",
                "network" => "MGD",
            ];
        }

        return $formated;
    }
}