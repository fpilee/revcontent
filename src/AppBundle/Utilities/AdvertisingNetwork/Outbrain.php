<?php


namespace AppBundle\Utilities\AdvertisingNetwork;


use AppBundle\Entity\Campaign;
use AppBundle\Entity\Setting;
use AppBundle\Utilities\Exceptions\ApiAuthenticationException;
use JMS\AopBundle\Exception\RuntimeException;
use Symfony\Component\Intl\Exception\NotImplementedException;

class Outbrain extends CampaignManager
{

    const CUSTOM_VARIABLE = "section";
    const AD_NETWORK = "Outbrain";


    public $token;
    public $client;
    public $secret;

    public function __construct(Setting $client, Setting $secret, Setting $token)
    {
        $this->token = $token;
        $this->client = $client;
        $this->secret = $secret;
    }

    private function reloadToken(){

        $client = new \GuzzleHttp\Client;

        $response = $client->request('GET', "https://api.outbrain.com/amplify/v0.1/login", [
            "http_errors" => false,
            'auth' => [
                $this->client->getValue(),
                $this->secret->getValue()
            ]
        ]);

        if($response->getStatusCode() !== 200){
            throw new ApiAuthenticationException("Outbrain token cannot be renewed, check credentials");
        }
        $content = json_decode($response->getBody()->getContents(), TRUE);

        $this->token->setValue($content["OB-TOKEN-V1"]);
    }

    public function getStats($campaignId, $start, $end)
    {
        $client = new \GuzzleHttp\Client;

        $startDate = $start->format("Y-m-d");
        $endDate = $end->format("Y-m-d");

        $response = $client->request('GET', "https://api.outbrain.com/amplify/v0.1/reports/marketers/00ad4b806ab5a781c9158a477b0b65b916/campaigns/sections?from=$startDate&to=$endDate&limit=500", [
            'headers' => [
                'OB-TOKEN-V1' => $this->token->getValue()
            ],
            "http_errors" => false,
        ]);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getStats(null,$start, $end);
        }


        $content = json_decode($response->getBody()->getContents(), TRUE);
        $results = $content["campaignResults"];
        $offset = 0;
        $count= 1;
        while($count > 0){
            $offset += 500;
            $response = $client->request('GET', "https://api.outbrain.com/amplify/v0.1/reports/marketers/00ad4b806ab5a781c9158a477b0b65b916/campaigns/sections?from=$startDate&to=$endDate&limit=500&offset=$offset", [
                'headers' => [
                    'OB-TOKEN-V1' => $this->token->getValue()
                ],
                "http_errors" => false,
            ]);


            $content_ = json_decode($response->getBody()->getContents(), TRUE);
            $results_ = $content_["campaignResults"];
            foreach ($results_ as $key_ => $campaign_){
                foreach ($results as $key => $campaign){
                    if($campaign["campaignId"] === $campaign_["campaignId"]){
                        $results[$key]["results"] = array_merge($campaign["results"], $campaign_["results"]);
                    }
                }
            }
            $count = $content_["totalCampaigns"];
        }


        return $this->formatStats($results, $start->format("m/d/Y"), $end->format("m/d/Y"));
    }

    public function updateCampaign($campaignId, $status)
    {
        $headers = [
            'OB-TOKEN-V1' => $this->token->getValue(),
            "Content-Type" => "application/json"
        ];
        $client = new \GuzzleHttp\Client(["http_errors" => false]);

        $validStatus = ["on" => true,"off"=> false];

        $body = ['enabled' => $validStatus[$status]];

        $request = new \GuzzleHttp\Psr7\Request(
            'PUT',
            "https://api.outbrain.com/amplify/v0.1/campaigns/$campaignId",
            $headers,
            json_encode($body)
        );

        $response = $client->send($request);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->updateCampaign($campaignId,$status);
        }
        else if($response->getStatusCode() === 429){
            //to many requests
            return false;
        }else if($response->getStatusCode() !== 200){
            //todo throw info
            throw new RuntimeException("Failed job " . $request->getBody()->getContents());
        }
        return true;
    }

    public function getCampaigns()
    {
        $client = new \GuzzleHttp\Client;


        $response = $client->request('GET', 'https://api.outbrain.com/amplify/v0.1/marketers/00ad4b806ab5a781c9158a477b0b65b916/campaigns', [
            'headers' => [
                'OB-TOKEN-V1' => $this->token->getValue()
            ],
            "http_errors" => false
        ]);


        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getCampaigns();
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);
        $campaigns = $content["campaigns"];
        if($content["count"] !== count($campaigns)){
            throw new NotImplementedException("Campaigns number is different from received");
        }

        $campaigns = $this->createCampaingsFromArray($campaigns);
        return $campaigns;

    }

    private function createCampaingsFromArray(array $campaignsArray){
        foreach($campaignsArray as $campaignArray){
            $campaign = new Campaign();
            $campaign->setName($campaignArray["name"]);
            $campaign->setId($campaignArray["id"]);
            $campaign->setSource("Outbrain");
            $campaigns[] = $campaign;
        }
        return $campaigns;
    }

    private function formatStats($data, $start, $end){

        foreach ($data as $datum){
            
            foreach ($datum["results"] as $ad){
                
                if(floatval($ad["metrics"]["spend"]) <= 0.0 ){
                    continue;
                }
                
                $formated []=[
                    "campaign_id" => $datum["campaignId"],
                    "start_date" => $start,
                    "end_date" => $end,
                    "ad_id" => $ad["metadata"]["id"],
                    "spend" => $ad["metrics"]["spend"],
                    "avg_cpc" =>$ad["metrics"]["ecpc"],
                    "clicks" =>$ad["metrics"]["clicks"],
                    "impressions" =>$ad["metrics"]["impressions"],
                    "custom_var" => "pubidx",
                    "network" => "OUT",
                ];
            }

        }
        return $formated;
    }

}