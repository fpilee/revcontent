<?php

namespace AppBundle\Utilities\AdvertisingNetwork;

use AppBundle\Entity\Campaign;
use AppBundle\Entity\Setting;
use GuzzleHttp;
use AppBundle\Utilities\Exceptions\ApiAuthenticationException;
use JMS\AopBundle\Exception\RuntimeException;


class RevContent extends CampaignManager
{
    const CUSTOM_VARIABLE = "widgid";
    const AD_NETWORK = "RevContent";
    public $client;
    public $secret;
    public $token;

    public function __construct(Setting $client, Setting $secret, Setting $token)
    {
        $this->client = $client;
        $this->secret = $secret;
        $this->token = $token;
    }


    public function getRawCampaigns($start, $end)
    {
        $headers = [
            'Authorization' => 'Bearer '. $this->token->getValue(),
            'Content-Type' => 'application/json',
            'Cache-Control' => 'no cache',
        ];
        $client = new GuzzleHttp\Client(["http_errors" => false]);

        $url = 'https://api.revcontent.io/stats/api/v1.0/boosts';


        $date_from = $start->format("Y-m-d");
        $date_to = $end->format("Y-m-d");


        $filters = [
            "date_from" => $date_from,
            "date_to" => $date_to,
            //"enabled" => "active",
            //"status" => "active" //active should be in the date from to but is today I think
        ];

        $params = http_build_query($filters);


        $response = $client->request('GET', $url . '?' . $params, [
            'headers'        => $headers,
        ]);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getRawCampaigns($start, $end);
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);
        return $content["data"];
    }

    public function getCampaigns()
    {
        return $this->createCampaingsFromArray($this->getRawCampaigns(new \DateTime(), new \DateTime()));
    }

    public function getRawStats($campaignId, $start, $end)
    {
        //todo use CURL for performance?
        $args = func_get_args();
        $offset = $args[3];
        $headers = [
            'Authorization' => 'Bearer '. $this->token->getValue(),
            'Content-Type' => 'application/json',
            'Cache-Control' => 'no cache',
        ];
        $client = new GuzzleHttp\Client(["http_errors" => false]);

        $url = "https://api.revcontent.io/stats/api/v1.0/boosts/$campaignId/widgets/stats";


        $date_from = $start->format("Y-m-d");
        $date_to = $end->format("Y-m-d");

        $filters = [
            "date_from" => $date_from,
            "date_to" => $date_to,
            "min_spend" => "0.01"
        ];

        if($offset > 0){
            $filters["offset"] = intval($offset);
        }

        $params = http_build_query($filters);


        $response = $client->request('GET', $url . '?' . $params, [
            'headers'        => $headers,
        ]);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->getRawStats($campaignId, $start, $end, $offset);
        }

        $content = json_decode($response->getBody()->getContents(), TRUE);
        return $content["data"];
    }

    public function getFormatedStat($data){

    }


    public function getStats($campaignId, $start, $end)
    {
        $date_from = $start->format("Y-m-d");
        $date_to = $end->format("Y-m-d");
        $boosts = $this->getRawCampaigns($start, $end);
        $campaingStats = [];
        foreach ($boosts as $boost){
            if(intval($boost["cost"])===0){
                continue;
            }
            $offset = 0;

            $stats = $this->getRawStats($boost["id"], $start, $end, $offset);
            //TODO implement this crap
            $aux = $stats;

            while(count($aux) === 100){
                $offset +=100;
                $aux = $this->getRawStats($boost["id"], $start, $end, $offset);
                $stats = array_merge($stats, $aux);
            }

            $campaingStats = array_merge($campaingStats, $this->formatStats($stats, $boost["id"], $start->format("m/d/Y"), $end->format("m/d/Y")));

        }
        return $campaingStats;

    }

    public function updateCampaign($campaignId, $status)
    {
        $headers = [
            'Authorization' => 'Bearer '. $this->token->getValue(),
            'Content-Type' => 'application/json',
            'Cache-Control' => 'no cache',
        ];
        $client = new GuzzleHttp\Client();

        $validStates = ["on" => "on","off"=> "off"]; //yep

        $body = ['id'=> $campaignId, 'enabled' => $validStates[$status]];

        $request = new GuzzleHttp\Psr7\Request(
            'POST',
            "https://api.revcontent.io/stats/api/v1.0/boosts",
            $headers,
            json_encode($body)
        );

        $response = $client->send($request);

        if($response->getStatusCode() === 401){
            $this->reloadToken();
            return $this->updateCampaign($campaignId,$status);
        }else if($response->getStatusCode() === 429){
            return false;
        }else if($response->getStatusCode() !== 200){
            throw new RuntimeException("Error changing status for rev" . $request->getBody()->getContents());
        }

        return true;
    }

    private function reloadToken(){

        $grant_type = "client_credentials";
        $client_id = $this->client->getValue();
        $client_secret = $this->secret->getValue();
        $data = compact('client_id', 'client_secret', 'grant_type');

        $args = http_build_query($data);

        $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Cache-Control' => 'no cache',
        ];

        $client = new GuzzleHttp\Client(["http_errors" => false]);

        $request = new GuzzleHttp\Psr7\Request(
            'POST',
            "https://api.revcontent.io/oauth/token",
            $headers,
            $args
        );

        $response = $client->send($request);

        if($response->getStatusCode() !== 200){
            throw new ApiAuthenticationException("Cannot authenticate Revcontent API or so");
        }
        $content = json_decode($response->getBody()->getContents(), TRUE);

        $this->token->setValue($content["access_token"]);
    }

    private function createCampaingsFromArray(array $campaignsArray){
        foreach($campaignsArray as $campaignArray){
            $campaign = new Campaign();
            $campaign->setName($campaignArray["name"]);
            $campaign->setId($campaignArray["id"]);
            $campaign->setSource("RevContent");
            $campaigns[] = $campaign;
        }
        return $campaigns;
    }

    private function formatStats($ads, $boostID, $start, $end){

        $formated =[];
        foreach ($ads as $ad){
            
            if(floatval($ad["spend"]) <= 0.0 ){
                continue;
            }

            $formated []=[
                "campaign_id" => $boostID,
                "ad_id" => $ad["id"],
                "start_date" => $start,
                "end_date" => $end,
                "impressions" =>$ad["impressions"],
                "clicks" =>$ad["clicks"],
                "spend" => $ad["spend"],
                "avg_cpc" => '',
                "custom_var" => "widgid",
                "network" => "RVC",
            ];
        }

        return $formated;
    }

    public function removeWidgetsFromBoost($boostId, $widgets)
    {
        $headers = [
            'Authorization' => 'Bearer '. $this->token->getValue(),
            'Content-Type' => 'application/json',
            'Cache-Control' => 'no cache',
        ];
        $client = new GuzzleHttp\Client(["http_errors" => false]);

        $url = "https://api.revcontent.io/stats/api/v1.0/boosts/$boostId/targets/optimizer/widgets/remove";

        if(strpos($widgets, "\r")){
            $widgets = explode("\r\n", $widgets); //win machine CRLF

        }else{
            $widgets = explode("\n", $widgets); //unix machine LF
        }
        $widgets = array_filter($widgets); //remove empty vars

        $widgets = implode(",", $widgets);

        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'json' => ['id' => $widgets]
            ]
        );

        $statusCode = $response->getStatusCode();
        $body = json_decode($response->getBody()->getContents(), true);
        if($response->getStatusCode() === 401){
            $this->reloadToken();
            $this->removeWidgetsFromBoost($boostId, $widgets);
        }
        if($statusCode === 200 and $body["success"]===true){
            return true;
        }
        return false;


    }

    public function getWidgetsFromBoost($boostId)
    {
        $headers = [
            'Authorization' => 'Bearer '. $this->token->getValue(),
            'Content-Type' => 'application/json',
            'Cache-Control' => 'no cache',
        ];
        $client = new GuzzleHttp\Client(["http_errors" => false]);

        $url = "https://api.revcontent.io/stats/api/v1.0/boosts/$boostId/targets/optimizer/widgets";

        $response = $client->request('GET', $url, [
                'headers' => $headers,
            ]
        );

        $statusCode = $response->getStatusCode();
        $body = json_decode($response->getBody()->getContents(), true);
        if($response->getStatusCode() === 401){
            $this->reloadToken();
            $this->getWidgetsFromBoost($boostId);
        }

        if($statusCode === 200 and isset($body["data"])){
            return $body["data"];
        }
        return null;

    }

}

