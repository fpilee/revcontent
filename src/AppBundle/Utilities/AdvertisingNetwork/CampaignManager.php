<?php

namespace AppBundle\Utilities\AdvertisingNetwork;

use AppBundle\Utilities\Interfaces\Updatable;
use AppBundle\Utilities\Interfaces\Reportable;
use AppBundle\Utilities\Interfaces\Manager;


abstract class CampaignManager implements Updatable, Reportable, Manager
{
    const CUSTOM_VARIABLE = 'customvar';
    const AD_NETWORK = 'adnetwork';

}