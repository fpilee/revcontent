<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 21/11/17
 * Time: 17:16
 */

namespace AppBundle\Utilities\Interfaces;


interface Manager
{
    public function getCampaigns();
}