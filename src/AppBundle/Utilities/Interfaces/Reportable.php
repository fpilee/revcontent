<?php

namespace AppBundle\Utilities\Interfaces;


interface Reportable
{
    public function getStats($campaignId,$start, $end);
}