<?php


namespace AppBundle\Utilities\Interfaces;


interface Updatable
{
    public function updateCampaign($campaignId, $status);
}