<?php

namespace AppBundle\Utilities\Interfaces;

use AppBundle\Entity\Token;

interface TokenManager {
    public function getAccessToken(Token $token);
}


