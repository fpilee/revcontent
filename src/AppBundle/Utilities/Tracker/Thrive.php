<?php

namespace AppBundle\Utilities\Tracker;


use AppBundle\Entity\Campaign;
use Symfony\Component\Config\Definition\Exception\Exception;

class Thrive
{

    const KEY = 'a233a833be8da034d3ff10a9641f0953';
    const SECRET = '13db3e02b5662724';

    /**
     * Make an temporary API access hash
     * @param string $key This installation's API Key
     * @param string $secret The API Secret generated above
     * @param int $offset Offset minutes between two servers
     * @return boolean|string FALSE on error | hash string on success
     */
    public static function makeApiHash($key, $secret, $offset = 0)
    {
        if (!is_string($key) || strlen($key) != 32) {
            return false;
        }
        if (!is_string($secret) || strlen($secret) != 16) {
            return false;
        }
        $now = time() + ($offset * 60);
        // hash changes every 60 seconds
        $date = (int)round(ceil(($now / 60)) * 60);

        return hash('sha256', md5($key . $secret . $date));
    }


    public static function getWidgetsMetrics($data)
    {

        $bool = date_default_timezone_set('EST');
        if(!$bool){
            throw new Exception("Cannot set EST as timezone");
        }

        $url = 'http://adtrker.com/ajax/otherVariables/getMetrics';
        $hash = Thrive::makeApiHash(Thrive::KEY, Thrive::SECRET);



        // The POST data '_9ad2314' is required
        // Any additional parameters needed by the method are added to the $postData
        $postData = array(
            '_9ad2314' => $hash
        );

        $postData = array_merge($postData, $data);

        $ch = curl_init();
        // if Curl has an issue verifying your servers SSL certificate, uncomment the next two lines
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        //curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));


        $jsonData = curl_exec($ch);

        curl_close($ch);

        // the returned data will be in $result
        $result = json_decode($jsonData, true);

        return $result;
    }



    public static function getCampaignsFor($source){
        $url = 'http://adtrker.com/ajax/sources/get';
        $hash = Thrive::makeApiHash(Thrive::KEY, Thrive::SECRET);

        //$hash = makeApiHash('a233a833be8da034d3ff10a9641f0953', '13db3e02b5662724');

        // The POST data '_9ad2314' is required
        // Any additional parameters needed by the method are added to the $postData
        $postData = array(
            '_9ad2314' => $hash
        );

        $ch = curl_init();
        // if Curl has an issue verifying your servers SSL certificate, uncomment the next two lines
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));

        $jsonData = curl_exec($ch);

        curl_close($ch);

        // the returned data will be in $result
        $result = json_decode($jsonData, true);

        if( $result["error"] === false){
            switch ($source){
                case "RevContent":
                    foreach ($result["data"] as $network){
                        if($network["name"] ===  "RevContent"){
                            return $network["camps"];
                        }
                    }
                    break;
                case "ContentAd":
                    foreach ($result["data"] as $network){
                        if($network["name"] ===  "Content.ad"){
                            return $network["camps"];
                        }
                    }
                    break;
                case "Outbrain":
                    foreach ($result["data"] as $network){
                        if($network["name"] ===  "OutBrain"){
                            return $network["camps"];
                        }
                    }
                    break;
                case "MGID":
                    foreach ($result["data"] as $network){
                        if($network["name"] ===  "MGID"){
                            return $network["camps"];
                        }
                    }
                    break;
                case "Taboola":
                    foreach ($result["data"] as $network){
                        if($network["name"] ===  "Taboola"){
                            return $network["camps"];
                        }
                    }
                    break;
                default:
                    return false;

            }

        }

        return false;


    }

    public static function getIdForName($campaigns, $thriveCampaigns){
        /**
         * @var Campaign $campaign
         */
        foreach ($campaigns as  $campaign){
            $maxPercent = 0;
            $maxPercentKey =-1;
            foreach ($thriveCampaigns as $key => $thriveCampaign){

                similar_text($campaign->getName() , $thriveCampaign["name"], $percent);
                if($percent > $maxPercent){
                    $maxPercent = $percent;
                    $maxPercentKey = $key;
                }
            }
            $campaignOriginName = $campaign->getName();
            $campaignExternalName = $thriveCampaigns[$maxPercentKey]["name"];
            if($maxPercent > 75) {
                $campaign->setThriveId($thriveCampaigns[$maxPercentKey]["campId"]);
                unset($thriveCampaigns[$maxPercentKey]);
            }
        }




        return 0;
    }
}