<?php


namespace AppBundle\Utilities\Tracker;


class NOIP
{

    const APISECRET = 'bd1d24280a8e5525af45f4504e063f68';

    private $token;
    private $ch;



    public function __construct()
    {

        $bool = date_default_timezone_set('UTC');
        if(!$bool){
            die("Can't set timezone as UTC");
        }

        $data = array('role' => 'api', 'username' => "deltawh", 'exp' => strtotime('+3 hour'));

        $this->token = NOIP::generateToken($data);
        $this->ch = NOIP::createCURLHandler($this->token);
    }


    public static function generateToken($payload) {
        $payload = json_encode($payload);
        $sig = hash_hmac('sha256', $payload, NOIP::APISECRET);
        return base64_encode($payload).'.'.base64_encode($sig);
    }

    public static function createCURLHandler($token){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt($ch, CURLOPT_HTTPHEADER,     ["Authorization: Bearer $token"] );
        return $ch;
    }

    public function getNOIPCampaigns($start,$end){

        $url = "https://learnnowhub.com/kjyrkjhoi/api/campaigns.php?a=list&from=$start&to=$end";
        $ch = $this->ch;

        curl_setopt($ch, CURLOPT_URL,            $url);

        $result = curl_exec ($ch);

        $data = json_decode($result, true);

        return $data;
    }

    public function getNOIPCampaign($campaign_id, $start, $end){

        $url = "http://learnnowhub.com/kjyrkjhoi/api/stats.php?a=params&clid=$campaign_id&from=$start&to=$end";

        $ch = $this->ch;
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec ($ch);
        $datacampaign= json_decode($result, true);

        return $datacampaign;

    }

}