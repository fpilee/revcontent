<?php

namespace AppBundle\Command;

use AppBundle\Entity\Campaign;
use AppBundle\Utilities\AdvertisingNetwork\CampaignManager;
use AppBundle\Utilities\AdvertisingNetwork\ContentAd;
use AppBundle\Utilities\AdvertisingNetwork\Outbrain;
use AppBundle\Utilities\AdvertisingNetwork\RevContent;
use AppBundle\Utilities\Interfaces\Updatable;
use Doctrine\ORM\EntityManager;
use SensioLabs\Security\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCampaignStatusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:update-campaign-status-for')
            ->addArgument('advertisingNetwork', InputArgument::REQUIRED, 'which Advertising Network?')
            ->addArgument('campaignId', InputArgument::REQUIRED, 'which Campaign Id?')
            ->addArgument('status', InputArgument::REQUIRED, 'what should I change?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!date_default_timezone_set('EST5EDT')){
            throw new RuntimeException("error setting timezone to EST, quitting get new boost command");
        }
        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $adNetwork =  $input->getArgument('advertisingNetwork');
        $campaignId =  $input->getArgument('campaignId');
        $status =  $input->getArgument('status');


        $token =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_token"]);
        $client =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_client"]);
        $secret =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"${adNetwork}_secret"]);

       /**
         * @var CampaignManager $manager
         */

        $classToLoad = "AppBundle\\Utilities\\AdvertisingNetwork\\$adNetwork";
        $manager = new $classToLoad($client, $secret, $token);


        //todo wait and re run if fail
        $attempts = 0;
        do{
            if($attempts > 0){
                sleep(60); //todo wait a minute?
            }
            $stats = $manager->updateCampaign($campaignId,$status);
            $attempts += 1;
        }while($stats === false && $attempts <= 5);

        $em->flush(); //if token entity was modified then we need to store it

    }
}