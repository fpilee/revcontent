<?php

namespace AppBundle\Command;

use AppBundle\Entity\Campaign;
use AppBundle\Utilities\AdvertisingNetwork\CampaignManager;
use AppBundle\Utilities\AdvertisingNetwork\ContentAd;
use AppBundle\Utilities\AdvertisingNetwork\Outbrain;
use AppBundle\Utilities\AdvertisingNetwork\RevContent;
use AppBundle\Utilities\Tracker\Thrive;
use AppBundle\Utilities\Interfaces\Updatable;
use Doctrine\ORM\EntityManager;
use SensioLabs\Security\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetNewCampaignsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:get-new-campaigns-for')
            ->addArgument('advertisingNetwork', InputArgument::REQUIRED, 'which Advertising Network?')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!date_default_timezone_set('EST5EDT')){
            throw new RuntimeException("error setting timezone to EST, quitting get new boost command");
        }
        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $adNetwork =  $input->getArgument('advertisingNetwork');

        $token =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_token"]);
        $client =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_client"]);
        $secret =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"${adNetwork}_secret"]);

       /**
         * @var CampaignManager $manager
         */

        $classToLoad = "AppBundle\\Utilities\\AdvertisingNetwork\\$adNetwork";
        $manager = new $classToLoad($client, $secret, $token);

        $thriveCampaigns = Thrive::getCampaignsFor($adNetwork);

        $campaigns = $manager->getCampaigns();

        Thrive::getIdForName($campaigns, $thriveCampaigns);
        /**
         * @var Campaign $campaign
         */
        foreach ($campaigns as $campaign){
            $duplicated = $em->getRepository("AppBundle:Campaign")->findOneBy(["id" => $campaign->getId()]);
            if($duplicated === null){
                $em->persist($campaign);
            }
        }
        $em->flush();
    }
}