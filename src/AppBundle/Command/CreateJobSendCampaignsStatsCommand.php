<?php

namespace AppBundle\Command;

use AppBundle\Utilities\AdvertisingNetwork\CampaignManager;
use Doctrine\ORM\EntityManager;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateJobSendCampaignsStatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:check-stats')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $adNetworks =["RevContent","ContentAd", "Outbrain", "MGID", "Taboola"];

        /**
         * @var CampaignManager $adNetwork
         */
        foreach ($adNetworks as $adNetwork){
            $job = new Job('app:send-campaigns-stats-for', [$adNetwork]);
            $em->persist($job);
        }

        $em->flush();
    }
}