<?php

namespace AppBundle\Command;

use AppBundle\Entity\Campaign;
use AppBundle\Entity\Rule;
use Doctrine\ORM\EntityManager;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use AppBundle\Entity\Boost;
use Symfony\Component\Intl\Exception\RuntimeException;

class CreateJobForProcessRulesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:create-job-for-process-rules')

            // the short description shown while running "php bin/console list"
            ->setDescription('Process Rules')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command saves jobs that run updates to campaigns')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!date_default_timezone_set('EST5EDT')){
            throw new RuntimeException("error setting timezone to EST, quitting get new boost command");
        }
        $logger = new Logger('my_logger');
        $logger->pushHandler(new StreamHandler(__DIR__.'/my_app.log', Logger::DEBUG));

        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $now = new \DateTime("now");
        $day = $now->format("D");
        $time = $now->format("H:i:00");

        $query = $em->createQuery("SELECT r FROM AppBundle:Rule r JOIN r.atDays d where r.atTime = '{$time}' and d.abbreviation = '{$day}' and r.enabled='1'");
        $rules = $query->getResult();

        /**
         * @var Rule $rule
         */
        foreach ($rules as $rule){
            $actionID = $rule->getAction()->getId();
            $campaigns = $rule->getCampaigns();

            /**
             * @var Campaign $campaign
             */
            foreach ($campaigns as $campaign){
                /**
                 * @var Boost $boost
                 */

                switch ($actionID){
                    case Rule::TURN_ON:
                        $job = new Job('app:update-campaign-status-for', [$campaign->getSource(), $campaign->getId(), 'on']);
                        $em->persist($job);
                        break;
                    case Rule::TURN_OFF:
                        $job = new Job('app:update-campaign-status-for', [$campaign->getSource(), $campaign->getId(), 'off']);
                        $em->persist($job);
                        break;
                    case Rule::INCREASE_BID:
                        /*$defaultBid=$boost->getDefaultBid();
                        $increase = $rule->getBidChange();
                        $newBid = $defaultBid + $increase;
                        $result = $revContent->updateBoostBid($accessToken, $boost->getBoostId(), $newBid);
                        $logger->info("setting boost id: {$boost->getBoostId()} from $defaultBid to $newBid", ["response" => $result]);*/
                        break;
                    case Rule::DECREASE_BID:
                        /*$defaultBid=$boost->getDefaultBid();
                        $decrease = $rule->getBidChange();
                        $newBid = $defaultBid - $decrease;
                        $result = $revContent->updateBoostBid($accessToken, $boost->getBoostId(), $newBid);
                        $logger->info("setting boost id: {$boost->getBoostId()} from $defaultBid to $newBid", ["response" => $result]);*/
                        break;
                    case Rule::RESTORE_BID:
                        /*$defaultBid = $boost->getDefaultBid();
                        $result = $revContent->updateBoostBid($accessToken, $boost->getBoostId(), $defaultBid);
                        $logger->info("restoring boost id: {$boost->getBoostId()} bid to $defaultBid", ["response" => $result]);*/
                        break;
                    default:
                        $logger->warn("invalid action in process rules cmd");
                        break;
                }
            }

        }
        $em->flush();
    }
}
