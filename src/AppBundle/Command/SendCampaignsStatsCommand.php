<?php

namespace AppBundle\Command;

use AppBundle\Entity\Campaign;
use AppBundle\Utilities\AdvertisingNetwork\CampaignManager;
use AppBundle\Utilities\AdvertisingNetwork\ContentAd;
use AppBundle\Utilities\AdvertisingNetwork\Outbrain;
use AppBundle\Utilities\AdvertisingNetwork\RevContent;
use AppBundle\Utilities\Interfaces\Updatable;
use Doctrine\ORM\EntityManager;
use SensioLabs\Security\Exception\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendCampaignsStatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:send-campaigns-stats-for')
            ->addArgument('advertisingNetwork', InputArgument::REQUIRED, 'which Advertising Network?')
            ->addArgument('date', InputArgument::OPTIONAL, 'which date?')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!date_default_timezone_set('EST5EDT')){
            throw new RuntimeException("error setting timezone to EST, quitting get new boost command");
        }
        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $adNetwork =  $input->getArgument('advertisingNetwork');
        $desiredDate =  $input->getArgument('date');

        $token =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_token"]);
        $client =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_client"]);
        $secret =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"${adNetwork}_secret"]);

       /**
         * @var CampaignManager $manager
         */

        $classToLoad = "AppBundle\\Utilities\\AdvertisingNetwork\\$adNetwork";
        $manager = new $classToLoad($client, $secret, $token);

        $defaultDateToUse = "-2 days";
        if($desiredDate != null){
            $defaultDateToUse = "-{$desiredDate} days";
        }

        $date = new \DateTime("");
        $date->modify($defaultDateToUse);
        $start = $end = $date;

        $stats = $manager->getStats(null,$start, $end);

        //todo use a inmemory stream
        $csvFilename = __DIR__."/cpcupdate$adNetwork.csv";
        $fh = fopen($csvFilename, "w");
        if(!$fh){
            die("permissions needed to write csv file!");
        }

        $bool = fputcsv($fh, ["Timezone","Start Date","End Date","CPC","Total","Traffic Source","Custom Variable","Custom Variable Value","Campaign ID","Campaign Name"]);

        /**
         * @var Campaign $campaign
         */
        $campaigns = $em->getRepository("AppBundle:Campaign")->findAll();
        foreach ($campaigns as $campaign){
            $thriveId = intval($campaign->getThriveId());
            if(!$thriveId>0){
                continue;
            }
            foreach ($stats as $stat){
                if($stat["campaign_id"] != $campaign->getId()){
                    continue;
                }

                $bool = fputcsv($fh, ["US/Eastern", $stat["start_date"], $stat["end_date"], $stat["avg_cpc"], $stat["spend"], $stat["network"], $stat["custom_var"], $stat["ad_id"], $thriveId, ""]);
            }
        }


        $bool =fclose($fh);

        $mailer = $this->getContainer()->get("mailer");
        $message = (new \Swift_Message("$adNetwork CSV file for custom variable update - " . $start->format("Y-m-d")))
            ->setFrom('schedulertool@learnnowhub.com')
            ->setTo(['marlonaphillips@gmail.com','stefan.hansmann@defendmedia.net','luh@domaindiscounter.com', '245172059@qq.com'])
            //->setBcc(["fpilee@gmail.com"])
            ->setBody(
                "File attached"
                ,
                'text/plain'
            )
            ->attach(\Swift_Attachment::fromPath($csvFilename))
        ;

        $status = $mailer->send($message);


        $em->flush();

    }
}
