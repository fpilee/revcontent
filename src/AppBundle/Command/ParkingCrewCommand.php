<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Utilities\RevContent;
use AppBundle\Entity\Boost;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class ParkingCrewCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:parking-crew')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
       

        if(!date_default_timezone_set('EST')){
            $logger->err("error setting timezone to EST, quitting get new boost command");
            return -1;
        }
       
        $user = "defendmedia";
        $api_key = "4bf577e0de231b76f348b506afb481f84ddc5960";

    
        $url = "https://api.parkingcrew.com/v4/public/%s/%s/tracking/epc/%s/%s";

        $date = new \DateTime();
        $date->modify("-3 days");
        $start = $end = $date->format("Y-m-d");
        $callurl = sprintf($url,$user, $api_key, $start, $end );

        // Crea un nuevo recurso cURL
        $ch = curl_init();

        // Establece la URL y otras opciones apropiadas
        curl_setopt($ch, CURLOPT_URL, $callurl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Captura la URL y la envía al navegador
        $response = curl_exec($ch);

        //$info = curl_getinfo($ch);

        curl_close($ch);
        
        $data = json_decode($response, TRUE);
                
    

        $csvFilename = __DIR__.'/subidcpcupdate.csv';
        $fh = fopen($csvFilename, "w");
        if(!$fh){
            die("permissions needed to write csv file!");
        }


        foreach ($data as $datum){
            $stat = fputcsv($fh, [$datum["date"], $datum["category"]["subid1"] , $datum["revenueUsd"]]);
        }


        $z =fclose($fh);

        $mailer = $this->getContainer()->get("mailer");
        $message = (new \Swift_Message("Parking crew subid file $start"))
            ->setFrom('schedulertool@learnnowhub.com')
            ->setTo(['marlon.phillips@defendmedia.net','stefan.hansmann@defendmedia.net','luh@domaindiscounter.com'])
            //->setBcc(["fpilee@gmail.com"])
            ->setBody(
                "File attached"
                ,
                'text/plain'
            )
            ->attach(\Swift_Attachment::fromPath($csvFilename))
        ;

        $status = $mailer->send($message);


        
    }



}



