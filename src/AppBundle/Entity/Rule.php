<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rule
 *
 * @ORM\Table(name="rule")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RuleRepository")
 * @ORM\HasLifecycleCallbacks
 *
 */
class Rule
{

    const TURN_ON = 1;
    const TURN_OFF = 2;
    const INCREASE_BID = 3;
    const DECREASE_BID = 4;
    const RESTORE_BID = 5;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="at_time", type="time")
     */
    private $atTime;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Days")
     * @ORM\JoinTable(name="rule_days",
     *      joinColumns={@ORM\JoinColumn(name="rule_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="day_id", referencedColumnName="id")}
     *      )
     */
    private $atDays;

    /**
     * @ORM\ManyToOne(targetEntity="Action", cascade="persist")
     * @ORM\JoinColumn(name="action", referencedColumnName="id")
     * */
    private $action;


    /**
     * @ORM\ManyToMany(targetEntity="Campaign", inversedBy="rules")
     *
     */
    private $campaigns;


    /**
     * @var \boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = true;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Rule
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Rule
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set atTime
     *
     * @param \DateTime $atTime
     *
     * @return Rule
     */
    public function setAtTime($atTime)
    {
        $this->atTime = $atTime;

        return $this;
    }

    /**
     * Get atTime
     *
     * @return \DateTime
     */
    public function getAtTime()
    {
        return $this->atTime;
    }



    /**
     *
     *
     * @ORM\PrePersist
     */
    public function onCreate()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }
    /**
     * Estableciendo fecha de actualización
     *
     * @ORM\PreUpdate
     */
    public function onUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Set action
     *
     * @param \AppBundle\Entity\Action $action
     *
     * @return Rule
     */
    public function setAction(\AppBundle\Entity\Action $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \AppBundle\Entity\Action
     */
    public function getAction()
    {
        return $this->action;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->atDays = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Add atDay
     *
     * @param \AppBundle\Entity\Days $atDay
     *
     * @return Rule
     */
    public function addAtDay(\AppBundle\Entity\Days $atDay)
    {
        $this->atDays[] = $atDay;

        return $this;
    }

    /**
     * Remove atDay
     *
     * @param \AppBundle\Entity\Days $atDay
     */
    public function removeAtDay(\AppBundle\Entity\Days $atDay)
    {
        $this->atDays->removeElement($atDay);
    }

    /**
     * Get atDays
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAtDays()
    {
        return $this->atDays;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Rule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Rule
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }



    /**
     * Add campaign
     *
     * @param \AppBundle\Entity\Campaign $campaign
     *
     * @return Rule
     */
    public function addCampaign(\AppBundle\Entity\Campaign $campaign)
    {
        $this->campaigns[] = $campaign;
        $campaign->addRule($this);
        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \AppBundle\Entity\Campaign $campaign
     */
    public function removeCampaign(\AppBundle\Entity\Campaign $campaign)
    {
        $this->campaigns->removeElement($campaign);
        $campaign->removeRule($this);
    }

    /**
     * Get campaigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }
}
