<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Widget
 *
 * @ORM\Table(name="widget")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WidgetRepository")
 */
class Widget
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * Many Widgets have Many Boosts.
     * @ORM\ManyToMany(targetEntity="Boost", mappedBy="widgets")
     */
    private $boosts;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Widget
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Widget
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->boosts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add boost
     *
     * @param \AppBundle\Entity\Boost $boost
     *
     * @return Widget
     */
    public function addBoost(\AppBundle\Entity\Boost $boost)
    {
        $this->boosts[] = $boost;

        return $this;
    }

    /**
     * Remove boost
     *
     * @param \AppBundle\Entity\Boost $boost
     */
    public function removeBoost(\AppBundle\Entity\Boost $boost)
    {
        $this->boosts->removeElement($boost);
    }

    /**
     * Get boosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoosts()
    {
        return $this->boosts;
    }
}
