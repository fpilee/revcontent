<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boost
 *
 * @ORM\Table(name="boost")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoostRepository")
 * @ORM\HasLifecycleCallbacks
 *
 */
class Boost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="boost_id", type="integer")
     */
    private $boostId;

    /**
     * @var int
     *
     * @ORM\Column(name="thrive_campaign_id", type="integer", nullable=true)
     */
    private $thriveCampaignId;

    /**
     * @var int
     *
     * @ORM\Column(name="noip_campaign_id", type="integer", nullable=true)
     */
    private $noipCampaignId;

    /**
     * @var string
     *
     * @ORM\Column(name="enabled", type="string", length=255)
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var float
     *
     * @ORM\Column(name="default_bid", type="float")
     */
    private $defaultBid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Rule", inversedBy="boosts")
     * @ORM\JoinTable(name="boost_rule")
     */
    private $rules;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Widget", inversedBy="boosts")
     * @ORM\JoinTable(name="boost_widget")
     */
    private $widgets;

    /**
     * @var \boolean
     *
     * @ORM\Column(name="hide", type="boolean")
     */
    private $hide = false;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Boost
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set boostId
     *
     * @param integer $boostId
     *
     * @return Boost
     */
    public function setBoostId($boostId)
    {
        $this->boostId = $boostId;

        return $this;
    }

    /**
     * Get boostId
     *
     * @return int
     */
    public function getBoostId()
    {
        return $this->boostId;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Boost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Boost
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param array $boostData

     * Constructor
     */
    public function __construct(array $boostData)
    {
        $this->rules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->boostId = $boostData["id"];
        $this->name = $boostData["name"];
        $this->startDate = new \DateTime($boostData["start_date"]);
        if( $boostData["end_date"] === null){
            $this->endDate = null;
        }else{
            $this->endDate =  new \DateTime($boostData["end_date"]);
        }

        $this->defaultBid = floatval($boostData["default_bid"]);
        $this->enabled = $boostData["enabled"];
        $this->status = $boostData["status"];
    }

    /**
     * Add rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return Boost
     */
    public function addRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rules[] = $rule;

        return $this;
    }

    /**
     * Remove rule
     *
     * @param \AppBundle\Entity\Rule $rule
     */
    public function removeRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rules->removeElement($rule);
    }

    /**
     * Get rules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Set enabled
     *
     * @param string $enabled
     *
     * @return Boost
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Boost
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Boost
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Boost
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set defaultBid
     *
     * @param float $defaultBid
     *
     * @return Boost
     */
    public function setDefaultBid($defaultBid)
    {
        $this->defaultBid = $defaultBid;

        return $this;
    }

    /**
     * Get defaultBid
     *
     * @return float
     */
    public function getDefaultBid()
    {
        return $this->defaultBid;
    }



    /**
     *
     *
     * @ORM\PrePersist
     */
    public function onCreate()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }
    /**
     * Estableciendo fecha de actualización
     *
     * @ORM\PreUpdate
     */
    public function onUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * Set hide
     *
     * @param boolean $hide
     *
     * @return Boost
     */
    public function setHide($hide)
    {
        $this->hide = $hide;

        return $this;
    }

    /**
     * Get hide
     *
     * @return boolean
     */
    public function getHide()
    {
        return $this->hide;
    }

    /**
     * @param array $data
     */
    public function updateEnabledAndStatus($data)
    {
        $this->setEnabled($data["enabled"]);
        $this->setStatus($data["status"]);
        $this->setDefaultBid(floatval($data["default_bid"]));
    }

    /**
     * Set thriveCampaignId
     *
     * @param integer $thriveCampaignId
     *
     * @return Boost
     */
    public function setThriveCampaignId($thriveCampaignId)
    {
        $this->thriveCampaignId = $thriveCampaignId;

        return $this;
    }

    /**
     * Get thriveCampaignId
     *
     * @return integer
     */
    public function getThriveCampaignId()
    {
        return $this->thriveCampaignId;
    }

    /**
     * Add widget
     *
     * @param \AppBundle\Entity\Widget $widget
     *
     * @return Boost
     */
    public function addWidget(\AppBundle\Entity\Widget $widget)
    {
        $this->widgets[] = $widget;

        return $this;
    }

    /**
     * Remove widget
     *
     * @param \AppBundle\Entity\Widget $widget
     */
    public function removeWidget(\AppBundle\Entity\Widget $widget)
    {
        $this->widgets->removeElement($widget);
    }

    /**
     * Get widgets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWidgets()
    {
        return $this->widgets;
    }

    /**
     * Set noipCampaignId
     *
     * @param integer $noipCampaignId
     *
     * @return Boost
     */
    public function setNoipCampaignId($noipCampaignId)
    {
        $this->noipCampaignId = $noipCampaignId;

        return $this;
    }

    /**
     * Get noipCampaignId
     *
     * @return integer
     */
    public function getNoipCampaignId()
    {
        return $this->noipCampaignId;
    }
}
