<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BoostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, array("disabled" => true))
            ->add('boostId', null, array("disabled" => true))
            ->add('thriveCampaignId', null)
            ->add('noipCampaignId', null)
            ->add('defaultBid', null, array("disabled" => true))
            ->add('rules', EntityType::class, array('class' => 'AppBundle:Rule',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
            ))

            ->add('hide')

        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Boost'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_boost';
    }


}
