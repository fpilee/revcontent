<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RuleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')

            ->add('atDays', EntityType::class, array('class' => 'AppBundle:Days',
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
            ))
            ->add('atTime')

            ->add('Campaigns', EntityType::class, array('class' => 'AppBundle:Campaign',
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => true,
                'required' => false
            ))
            ->add('action', EntityType::class, array('class' => 'AppBundle:Action',
                'choice_label' => 'name',
                ))

            ->add('enabled')

        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Rule'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rule';
    }


}
