<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Boost;
use AppBundle\Utilities\NOIP;
use AppBundle\Utilities\RevContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Boost controller.
 *
 * @Route("boost")
 */
class BoostController extends Controller
{
    /**
     * Lists all boost entities.
     *
     * @Route("/", name="boost_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT b FROM AppBundle:Boost b";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/,
            array('defaultSortFieldName' => 'b.id', 'defaultSortDirection' => 'desc')

        );
        return $this->render('boost/index.html.twig', array('pagination' => $pagination));

    }

    /**
     * Creates a new boost entity.
     *
     * @Route("/new", name="boost_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $boost = new Boost();
        $form = $this->createForm('AppBundle\Form\BoostType', $boost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($boost);
            $em->flush();

            return $this->redirectToRoute('boost_show', array('id' => $boost->getId()));
        }

        return $this->render('boost/new.html.twig', array(
            'boost' => $boost,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new boost entity.
     *
     * @Route("/refreshboosts", name="boost_refresh")
     * @Method({"GET"})
     */
    public function refreshAction(Request $request)
    {
        /**
         * @var KernelInterface $kernel
         */
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(array(
            'command' => 'app:get-new-boosts'
        ));
        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput();
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $content = $output->fetch();

        $this->addFlash("info", "Done $content");

        // return new Response(""), if you used NullOutput()
        return $this->redirectToRoute("boost_index");

    }

    public function createSelectDateForm(){
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('boost_reports_select_boost'))
            ->setMethod('POST')
            ->add('start', DateType::class, array(
                'widget' => 'single_text',
            ))
            ->add('end', DateType::class, array(
                'widget' => 'single_text',
            ))
            //->add('save', SubmitType::class)
            ->getForm();
        return $form;
    }

    /**
     * Displays a form select report date range
     *
     * @Route("/reports/selectdate", name="boost_reports_select_date")
     * @Method({"GET"})
     */
    public function reportSelectDateAction(Request $request)
    {

        $form = $this->createSelectDateForm();

        return $this->render('widget/reports.html.twig', array(
            'form' => $form->createView(),
        ));

    }


    public function createBoostSelectForm($start, $end, $list){
        $form = $this->createFormBuilder()
            ->add('start', DateType::class, ['data' => $start, 'widget' => 'single_text'])
            ->add('end', DateType::class, ['data' => $end, 'widget' => 'single_text'])
            ->setAction($this->generateUrl('report_boost_show'))
            ->setMethod('GET')
            ->add('boost', ChoiceType::class, ['choices' => $list])
            //->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();
        return $form;
    }

    /**
     * Displays a form to select report boost
     *
     * @Route("/reports/selectboost", name="boost_reports_select_boost")
     * @Method({"POST"})
     */
    public function reportSelectBoostAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createSelectDateForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $start = $form->get("start")->getData();
            $end = $form->get("end")->getData();

            $tokenInfo = $em->getRepository('AppBundle:Token')->findOneBy(["id" => 1]);
            $tokenManager = new RevContent();
            $accessToken = $tokenManager->getAccessToken($tokenInfo);

            $response = $tokenManager->getActiveBoosts($accessToken, $start, $end);

            $aux = json_decode($response, true);

            $boostList = $aux["data"];

            foreach ($boostList as $boost){
                $boost['cost'] = round($boost["cost"], 3);
                $list[$boost["id"]] = "{$boost['id']} - {$boost['name']} - ({$boost['status']}) - COST: {$boost['cost']}";
            }
            $list = array_flip($list);

            $form = $this->createBoostSelectForm($start, $end, $list);

            return $this->render('widget/reports.html.twig', array(
                'form' => $form->createView(),
            ));

        }

    }

    /**
     * Displays the report
     *
     * @Route("/report/boost", name="report_boost_show")
     * @Method({"GET"})
     */
    public function reportBoostAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createBoostSelectForm(null, null, []);

        $form->handleRequest($request);

        $start = $form->get("start")->getData();
        $end = $form->get("end")->getData();
        $boost = $form->get("boost")->getViewData();
        /**
         * @var Boost $boostObj
         */
        $boostObj = $em->getRepository('AppBundle:Boost')->findOneBy(["boostId" => $boost]);
        if($boostObj === null){
            die("BOST NOT FOUND POW");
        }


        $tokenInfo = $em->getRepository('AppBundle:Token')->findOneBy(["id" => 1]);
        $tokenManager = new RevContent();
        $accessToken = $tokenManager->getAccessToken($tokenInfo);

        $offset = 0;
        $info = $tokenManager->getWidgetStats($accessToken,$boost,$offset, $start, $end);
        $info = json_decode($info, TRUE);
        $_info = $info;

        while(count($_info["data"]) === 100){
            $offset +=100;
            $_info = $tokenManager->getWidgetStats($accessToken,$boost,$offset, $start, $end);
            $_info = json_decode($_info, TRUE);
            $info["data"] = array_merge($info["data"], $_info["data"]);
        }


        /**
         * @var NOIP $c
         */
        $c = $this->get("app.noipcampaigns");

        $start = $start->format("Y-m-d");
        $end = $end->format("Y-m-d");

        $data = $c->getNOIPCampaigns($start, $end);

        $boostThriveId = $boostObj->getThriveCampaignId();

        //var_dump($boostThriveId);
        $thriveCampaignID = false;
        foreach ($data as $noipCampaign){
            $url = $noipCampaign["realurl"][0]["url"];
            $query = parse_url($url, PHP_URL_QUERY);
            parse_str($query, $queryData);

            if(!array_key_exists("trvid", $queryData)){
                continue;
            }
            $thriveId = intval($queryData["trvid"]);
            if($boostThriveId === $thriveId){
                $thriveCampaignID = $noipCampaign["name"];

                //var_dump($noipCampaign);
                //echo "$boostThriveId === $thriveId" . "\n";

            }else{
                //echo "$boostThriveId <> $thriveId" . "\n";
            }
        }
        $data = false;
        if($thriveCampaignID !== false){
            $data = $c->getNOIPCampaign($thriveCampaignID, $start, $end);
        }


        //var_dump($data["data"][3]["vals"]);die();

        $widgetsInfo = $data["data"][3]["vals"];

        if($info["success"] === true){
            if( count($info["data"]) > 0){
                $headers = array_keys($info["data"][0]);
                $headers = array_slice($headers, 0, 8);
                array_push($headers, "vctr");
                array_push($headers, "rvc");
                array_push($headers, "noip total");
                array_push($headers, "noip blocked");
                array_push($headers, "rvc-noip blocked");
                array_push($headers, "noipblocked/rvc");
                $rows = [];
                foreach ($info["data"] as $elem){
                    //copy array
                    $_elem = array_values($elem);
                    $elem_ = array_values($elem);

                    //round float fields
                    $aux = $elem_[5];
                    $rounded = round($aux, 3);
                    $_elem[5] = $rounded;

                    $aux = $elem_[6];
                    $rounded = round($aux, 3);
                    $_elem[6] = $rounded;

                    $aux = $elem_[7];
                    $rounded = round($aux, 3);
                    $_elem[7] = $rounded;

                    $viewableImpressions = floatval($elem_[2]);
                    $clicks = floatval($elem_[3]);
                    if($viewableImpressions == 0){
                        $ctrViewableImpressions = 0;
                    }else{
                        $ctrViewableImpressions = round(($clicks / $viewableImpressions)*100, 4);
                    }
                    $_elem[8] = $ctrViewableImpressions;

                    $_elem[9] =  $_elem[3] ;
                    $_elem[10] = "N/A";
                    $_elem[11] = "N/A";
                    //$_elem[12] = "N/A";
                    foreach ($widgetsInfo as $widgetInfo){
                        if(intval($elem["id"]) === $widgetInfo["name"]){
                            $_elem[10] = $widgetInfo["total"];
                            $_elem[11] = $_elem[3] - $_elem[10];
                            //$_elem[12] = $_elem[3] / $_elem[9];
                        }
                    }

                    $rows[]= array_slice($_elem, 0, 13);
                }
            }
        }


        /*$response = new JsonResponse($data);
        return $response;*/


        return $this->render('widget/report_show.html.twig', array(
            "headers" => $headers,
            "rows" => $rows,
        ));

    }

    /**
     * Displays a form select report date range
     *
     * @Route("/reports", name="boost_reports")
     * @Method({"GET", "POST"})
     */
    public function reportSelectAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('AppBundle\Form\WidgetsReportType');

        /**
         * @var NOIP $c
         */
        /*$c = $this->get("app.noipcampaigns");
        $start = new \DateTime("now");
        $start = $end = $start->format("Y-m-d");

        $data = $c->getNOIPCampaigns($start, $end);

        $response = new JsonResponse($data);


        return $response;*/
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $start = $form->get("start")->getData();
            $end = $form->get("end")->getData();

            $tokenInfo = $em->getRepository('AppBundle:Token')->findOneBy(["id" => 1]);
            $tokenManager = new RevContent();
            $accessToken = $tokenManager->getAccessToken($tokenInfo);

            $response = $tokenManager->getActiveBoosts($accessToken, $start, $end);

            $aux = json_decode($response, true);

            $boostList = $aux["data"];

            //var_dump($boostList);die();
            foreach ($boostList as $boost){
                $boost['cost'] = round($boost["cost"], 3);
                $list[$boost["id"]] = "{$boost['id']} - {$boost['name']} - {$boost['status']} cost:{$boost['cost']}";
            }
            $list = array_flip($list);

            $form = $this->createFormBuilder()
                ->add('boost', ChoiceType::class, ['choices' => $list])
                //->add('save', SubmitType::class, array('label' => 'Create Task'))
                ->getForm();

            return $this->render('widget/reports.html.twig', array(
                'form' => $form->createView(),
            ));

            return new JsonResponse(["boosts" => $boostList ]);
        }


        return $this->render('widget/reports.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * Finds and displays a boost entity.
     *
     * @Route("/{id}", name="boost_show")
     * @Method("GET")
     */
    public function showAction(Boost $boost)
    {
        $deleteForm = $this->createDeleteForm($boost);

        return $this->render('boost/show.html.twig', array(
            'boost' => $boost,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing boost entity.
     *
     * @Route("/{id}/edit", name="boost_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Boost $boost)
    {
        $deleteForm = $this->createDeleteForm($boost);
        $editForm = $this->createForm('AppBundle\Form\BoostType', $boost);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('boost_edit', array('id' => $boost->getId()));
        }

        return $this->render('boost/edit.html.twig', array(
            'boost' => $boost,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a boost entity.
     *
     * @Route("/{id}", name="boost_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Boost $boost)
    {
        $form = $this->createDeleteForm($boost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($boost);
            $em->flush();
        }

        return $this->redirectToRoute('boost_index');
    }

    /**
     * Creates a form to delete a boost entity.
     *
     * @param Boost $boost The boost entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Boost $boost)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('boost_delete', array('id' => $boost->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
