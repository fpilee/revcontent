<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Campaign;
use AppBundle\Utilities\AdvertisingNetwork\CampaignManager;
use AppBundle\Utilities\Tracker\Thrive;
use Doctrine\Common\Cache\ApcuCache;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Intl\Exception\NotImplementedException;

/**
 * Campaign controller.
 *
 * @Route("stats")
 */
class StatsController extends Controller
{

    /**
     *
     * @Route("/thrivecostupdate", name="thrive_cost_update_form")
     * @Method({"GET"})
     */
    public function thriveCostUpdateAction(Request $request){


        $start = new \Datetime($request->query->get("start"));
        $end = new \Datetime($request->query->get("end"));
        $adNetwork = $request->query->get("adNetwork");

        if(empty($adNetwork)){
            return $this->render('stat/thrive_cost_update_form.html.twig');
        }

        $bool = date_default_timezone_set('EST');
        if(!$bool){
            throw new NotFoundHttpException("Cannot set EST as timezone");
        }

        $cacheDriver = new ApcuCache();
        $key = "{$adNetwork}_ads_csv_{$start->format('Y-m-d')}_{$end->format('Y-m-d')}";
        if ($cacheDriver->contains($key)) {
            $segments = $cacheDriver->fetch($key);

            return $this->render('stat/thrive_cost_update_table.html.twig', array(
                'segments' => $segments,
                'network' => $adNetwork,
                'start' => $start,
                'end' => $end,
            ));
        }
        $em = $this->getDoctrine()->getManager();

        $token =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_token"]);
        $client =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_client"]);
        $secret =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"${adNetwork}_secret"]);

        /**
         * @var CampaignManager $manager
         */

        $classToLoad = "AppBundle\\Utilities\\AdvertisingNetwork\\$adNetwork";
        $manager = new $classToLoad($client, $secret, $token);


        $stats = $manager->getStats(null,$start, $end);
        $em->flush();
        $campaigns = $em->getRepository("AppBundle:Campaign")->findAll();
        /**
         * @var Campaign $campaign
         */
        foreach ($campaigns as $campaign) {
            $thriveId = $campaign->getThriveId();
            if ($thriveId === null) {
                continue;
            }
            foreach ($stats as $stat){
                if($stat["campaign_id"] != $campaign->getId()){
                    continue;
                }

                $segments[] = ["US/Eastern", $stat["start_date"], $stat["end_date"], $stat["avg_cpc"], $stat["spend"], $stat["network"], $stat["custom_var"], $stat["ad_id"], $thriveId, ""];
            }
        }

        $cacheDriver->save($key, $segments, 60*15);


        return $this->render('stat/thrive_cost_update_table.html.twig', array(
            'segments' => $segments,
            'network' => $adNetwork,
            'start' => $start,
            'end' => $end,
        ));
    }


    /**
     * Creates a new widget entity.
     *
     * @Route("/thriveclicks", name="thrive_clicks_display_form")
     * @Method({"GET", "POST"})
     */
    public function thriveClicksAction(Request $request)
    {

        $start = new \Datetime($request->query->get("start"));
        $end = new \Datetime($request->query->get("end"));
        $adNetwork = $request->query->get("adNetwork");

        if(empty($adNetwork)){
            return $this->render('stat/thrive_clicks_form.html.twig');
        }


        $key = "{$adNetwork}_ads_clicks_cmp_{$start->format('Y-m-d')}_{$end->format('Y-m-d')}";


        $cacheDriver = new ApcuCache();

        if ($cacheDriver->contains($key)) {
            $stats = $cacheDriver->fetch($key);
            return $this->render('stat/thrive_clicks_table.html.twig', array(
                'stats' => $stats,
                'network' => $adNetwork,
                'start' => $start,
                'end' => $end,
            ));
        }

        $bool = date_default_timezone_set('EST');
        if(!$bool){
            throw new NotFoundHttpException("Cannot set EST as timezone");
        }

        $em = $this->getDoctrine()->getManager();

        $token =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_token"]);
        $client =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_client"]);
        $secret =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"${adNetwork}_secret"]);

        /**
         * @var CampaignManager $manager
         */

        $classToLoad = "AppBundle\\Utilities\\AdvertisingNetwork\\$adNetwork";
        $manager = new $classToLoad($client, $secret, $token);


        $stats = $manager->getStats(null,$start, $end);

        $campaigns = $em->getRepository("AppBundle:Campaign")->findAll();
        /**
         * @var Campaign $campaign
         */
        foreach ($campaigns as $campaign){
            $campaignId = $campaign->getThriveId();
            if($campaignId === null){
                continue;
            }

            $thrive_args = [
                "range" => [
                    "from" => "{$start->format('m/d/Y')} 00:00",
                    "to" => "{$end->format('m/d/Y')} 23:59",
                    "timezone" => "US/Eastern"
                ],
                "camps" => $campaignId,
                "key" => "custom." . $manager::CUSTOM_VARIABLE
            ];

            $adsClickInThrive = Thrive::getWidgetsMetrics($thrive_args);

            foreach ($stats as $key => $stat){
                if($stat["campaign_id"] != $campaign->getId()){
                    continue;
                }
                foreach ($adsClickInThrive as $adClickInThrive ){
                    if($stat['ad_id'] == $adClickInThrive['value']){
                        $stats[$key]["thrive_clicks"] =  $adClickInThrive['clicks'];
                    }
                }

            }

        }

        $cacheDriver->save($key, $stats, 60*15);

        $em->flush();
        return $this->render('stat/thrive_clicks_table.html.twig', array(
            'stats' => $stats,
            'network' => $adNetwork,
            'start' => $start,
            'end' => $end,

        ));
    }
}
