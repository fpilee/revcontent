<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Boost;
use AppBundle\Entity\Widget;
use AppBundle\Utilities\RevContent;
use AppBundle\Utilities\Thrive;
use Doctrine\Common\Cache\ApcuCache;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Widget controller.
 *
 * @Route("widget")
 */
class WidgetController extends Controller
{
    /**
     * Lists all widget entities.
     *
     * @Route("/", name="widget_index")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $em    = $this->get('doctrine.orm.entity_manager');
        $dql   = "SELECT w FROM AppBundle:Widget w";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/,
            array('defaultSortFieldName' => 'w.id', 'defaultSortDirection' => 'desc')

        );

        return $this->render('widget/index.html.twig', array('pagination' => $pagination));

    }

    /**
     * Lists all widget entities.
     *
     * @Route("/widgetactions", name="widgetoptimizer_index")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetOptimizerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $boosts = $em->getRepository('AppBundle:Campaign')->findBy(["source" => "RevContent"]);
        return $this->render('widget/widgetoptimizer.html.twig', ["boosts" => $boosts]);

    }
    /**
     * Deletes a widget entity.
     *
     * @Route("/widgetactions", name="widgetptimizer_process")
     * @Method("POST")
     */
    public function widgetOptimizerProcessAction(Request $request)
    {
        $boostId = $request->request->get("boost");

        $em = $this->getDoctrine()->getManager();

        $boost = $em->getRepository('AppBundle:Campaign')->find($boostId);

        if($boost === null){
            //die("Failed to retrieve boost info");
            $this->addFlash("warning", "Warning failed to retrieve boost: $boostId info. Is not in database");
        }

        $widgets = $request->request->get("widgets");


        if(count($widgets) < 1){
            die("Failed empty widget list");
        }
        $adNetwork = "RevContent";
        $token =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_token"]);
        $client =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"{$adNetwork}_client"]);
        $secret =$em->getRepository("AppBundle:Setting")->findOneBy(["name" =>"${adNetwork}_secret"]);

        $classToLoad = "AppBundle\\Utilities\\AdvertisingNetwork\\$adNetwork";
        /**
         * @var RevContent $manager
         */
        $manager = new $classToLoad($client, $secret, $token);

        $success = $manager->removeWidgetsFromBoost($boostId, $widgets);

        if($success === true){
            $this->addFlash("success", "Removed widgets: " . $widgets . " from boost : $boostId");
        }else{
            $this->addFlash("error", "Failed to removed widgets: " .  $widgets . " from boost : $boostId");
        }

        return $this->redirectToRoute("widgetoptimizer_index");


    }

    /**
     * Creates a new widget entity.
     *
     * @Route("/display", name="widget_display")
     * @Method({"GET", "POST"})
     */
    public function displayAction(Request $request)
    {

        $start = new \Datetime($request->query->get("start"));
        $end = new \Datetime($request->query->get("end"));

        $key = "widgetcmp_{$start->format('Y-m-d')}_{$end->format('Y-m-d')}";


        $cacheDriver = new ApcuCache();

        if ($cacheDriver->contains($key)) {
            $widgets = $cacheDriver->fetch($key);

            return $this->render('widget/display.html.twig', array(
                'widgets' => $widgets,
                'start' => $start,
                'end' => $end,
            ));
        }

        $bool = date_default_timezone_set('EST');
        if(!$bool){
            throw new NotFoundHttpException("Cannot set EST as timezone");
        }


        $em = $this->getDoctrine()->getManager();
        $tokenInfo = $em->getRepository('AppBundle:Token')->findOneBy(["id" => 1]);
        $tokenManager = new RevContent();
        $accessToken = $tokenManager->getAccessToken($tokenInfo);

        $boosts = $em->getRepository("AppBundle:Boost")->findAll();

        $widgets = [];
        /**
         * @var Boost $boost
         */
        foreach ($boosts as $boost){

            if(!$boost->getThriveCampaignId() or $boost->getHide()){
                continue;
            }

            $offset = 0;
            $info = $tokenManager->getWidgetStats($accessToken, $boost->getBoostId(), $offset, $start, $end);
            $info = json_decode($info, true);
            $_info = $info;

            while(count($_info["data"]) === 100){
                $offset +=100;

                $_info =  $tokenManager->getWidgetStats($accessToken, $boost->getBoostId(), $offset,$start,$end); // ->
                $_info = json_decode($_info, true);

                $info["data"] = array_merge($info["data"], $_info["data"]);
            }


            $thriveData = [
                "range" => [
                    "from" => "{$start->format('m/d/Y')} 00:00",
                    "to" => "{$end->format('m/d/Y')} 23:59",
                    "timezone" => "US/Eastern"
                ],
                "camps" => $boost->getThriveCampaignId(),
                "key" => "custom.widgid"
            ];

            $widgetsInThrive = Thrive::getWidgetsMetrics($thriveData);

            foreach ($info["data"] as $keyWidget => $widgetData){

                foreach ($widgetsInThrive as $keyThrive => $widgetInThrive){
                    if($widgetInThrive["value"] === intval($widgetData["id"])){
                        $info["data"][$keyWidget]["clicks"] = intval($info["data"][$keyWidget]["clicks"]);
                        $info["data"][$keyWidget]["tr_clicks"] = intval($widgetInThrive["clicks"]);
                        unset($widgetsInThrive[$keyThrive]);
                        break;
                    }
                }


            }
            // merge widgets in thrive that are not in revcontent
            foreach ($widgetsInThrive as $keyThrive => $widgetInThrive){
                $info["data"][] = [
                    'id' => $widgetInThrive['value'],
                    'impressions' => 'N/A',
                    'viewable_impressions' => 'N/A',
                    'clicks' => 0,
                    'tr_clicks' => intval($widgetInThrive["clicks"]),
                    'ctr' => 'N/A',
                    'avg_cpc' => 'N/A',
                    'avg_cpv' => 'N/A',
                    'spend' => 'N/A',
                    'conversions' =>'N/A',
                    'profit' => 'N/A'
                ];
            }

            $widgets[$boost->getBoostId()] = $info["data"];
        }

        $cacheDriver->save($key, $widgets, 60*15);

        $em->flush();
        return $this->render('widget/display.html.twig', array(
            'widgets' => $widgets,
            'start' => $start,
            'end' => $end,
        ));
    }

    /**
     * Creates a new widget entity.
     *
     * @Route("/comparethrive", name="widget_compare_thrive")
     * @Method({"GET"})
     */
    public function displayFormCompareAction(Request $request){

        return $this->render('widget/compare_thrive_rev.html.twig');
    }

    /**
     * Creates a new widget entity.
     *
     * @Route("/thrivecsv", name="widget_thrive_csv_form")
     * @Method({"GET"})
     */
    public function displayFormCSVAction(Request $request){

        return $this->render('widget/thrive_form.html.twig');
    }

    /**
     * Creates a new widget entity.
     *
     * @Route("/thrivecsv/range", name="widget_thrive_csv")
     * @Method({"GET"})
     */
    public function widgetThriveCSVAction(Request $request)
    {

        $start = new \Datetime($request->query->get("start"));
        $end = new \Datetime($request->query->get("end"));


        $cacheDriver = new ApcuCache();
        $key = "widgetscsv_{$start->format('Y-m-d')}_{$end->format('Y-m-d')}";
        if ($cacheDriver->contains($key)) {
            $widgets = $cacheDriver->fetch($key);

            return $this->render('widget/thrive_csv.html.twig', array(
                'widgets' => $widgets,
            ));
        }

        $bool = date_default_timezone_set('EST');
        if(!$bool){
            throw new NotFoundHttpException("Cannot set EST as timezone");
        }


        $em = $this->getDoctrine()->getManager();
        $tokenInfo = $em->getRepository('AppBundle:Token')->findOneBy(["id" => 1]);
        $tokenManager = new RevContent();
        $accessToken = $tokenManager->getAccessToken($tokenInfo);

        $boosts = $em->getRepository("AppBundle:Boost")->findAll();



        $widgets = [];
        /**
         * @var Boost $boost
         */
        foreach ($boosts as $boost){

            if(!$boost->getThriveCampaignId() or $boost->getHide()){
                continue;
            }


            $offset = 0;
            $info = $tokenManager->getWidgetStats($accessToken, $boost->getBoostId(), $offset, $start, $end);
            $info = json_decode($info, true);
            $_info = $info;

            while(count($_info["data"]) === 100){
                $offset +=100;

                $_info =  $tokenManager->getWidgetStats($accessToken, $boost->getBoostId(), $offset,$start,$end); // ->
                $_info = json_decode($_info, true);

                $info["data"] = array_merge($info["data"], $_info["data"]);
            }


            foreach ($info["data"] as $widgetData){

                $widget_id = $widgetData["id"];
                $avg_cpc = floatval($widgetData["avg_cpc"]);
                if($avg_cpc > 0) {
                    $spend = floatval($widgetData["spend"]);

                    $startDate = $start->format("m/d/Y");
                    $endDate = $end->format("m/d/Y");

                    $widgets[] = ["US/Eastern", "$startDate", "$endDate", "", "$spend", "RVC", "widgid", "$widget_id", "{$boost->getThriveCampaignId()}", ""];
                }

            }



        }

        $cacheDriver->save($key, $widgets, 60*15);

        $em->flush();
        return $this->render('widget/thrive_csv.html.twig', array(
            'widgets' => $widgets,
        ));
    }

    /**
     * Creates a new widget entity.
     *
     * @Route("/new", name="widget_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $widget = new Widget();
        $form = $this->createForm('AppBundle\Form\WidgetType', $widget);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($widget);
            $em->flush();

            return $this->redirectToRoute('widget_show', array('number' => $widget->getNumber()));
        }

        return $this->render('widget/new.html.twig', array(
            'widget' => $widget,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a widget entity.
     *
     * @Route("/{number}", name="widget_show")
     * @Method("GET")
     */
    public function showAction(Widget $widget)
    {
        $deleteForm = $this->createDeleteForm($widget);

        return $this->render('widget/show.html.twig', array(
            'widget' => $widget,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing widget entity.
     *
     * @Route("/{number}/edit", name="widget_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Widget $widget)
    {
        $deleteForm = $this->createDeleteForm($widget);
        $editForm = $this->createForm('AppBundle\Form\WidgetType', $widget);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('widget_edit', array('number' => $widget->getNumber()));
        }

        return $this->render('widget/edit.html.twig', array(
            'widget' => $widget,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a widget entity.
     *
     * @Route("/{number}", name="widget_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Widget $widget)
    {
        $form = $this->createDeleteForm($widget);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($widget);
            $em->flush();
        }

        return $this->redirectToRoute('widget_index');
    }

    /**
     * Creates a form to delete a widget entity.
     *
     * @param Widget $widget The widget entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Widget $widget)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('widget_delete', array('number' => $widget->getNumber())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
